# GitHub REST API documentation

*31 mai 2024*

<https://docs.github.com/en/rest?apiVersion=2022-11-28>, `apiVersion=2022-11-28` insérée automatiquement

## Quickstart

### GitHub CLI

Présenté par défaut.

<https://docs.github.com/en/rest/quickstart?apiVersion=2022-11-28>

> GitHub CLI automatically stores your Git credentials for you when you choose
> HTTPS as your preferred protocol for Git operations and answer "yes" to the
> prompt asking if you would like to authenticate to Git with your GitHub
> credentials.

Mais il le stocke où ???

### curl

<https://docs.github.com/en/rest/quickstart?apiVersion=2022-11-28&tool=curl>

Ils sont lourds avec leur **GitHub CLI**, à quoi bon une API si on ne peut pas
utiliser les outils de notre choix ?

```shell
$ curl --version
curl 7.81.0 (x86_64-pc-linux-gnu) libcurl/7.81.0 OpenSSL/3.0.2 zlib/1.2.11 brotli/1.0.9 zstd/1.4.8 libidn2/2.3.2 libpsl/0.21.0 (+libidn2/2.3.2) libssh/0.9.6/openssl/zlib nghttp2/1.43.0 librtmp/2.3 OpenLDAP/2.5.17
Release-Date: 2022-01-05
Protocols: dict file ftp ftps gopher gophers http https imap imaps ldap ldaps mqtt pop3 pop3s rtmp rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: alt-svc AsynchDNS brotli GSS-API HSTS HTTP2 HTTPS-proxy IDN IPv6 Kerberos Largefile libz NTLM NTLM_WB PSL SPNEGO SSL TLS-SRP UnixSockets zstd
```

Pourquoi est-ce un `curl` de 2022 alors que l'on est en 2024, certes sur une
Ubuntu LTS 2022 mais pourquoi le package n'a-t-il pas été upgradé ?

> Warning: Treat your access token like a password.
>
> To keep your token secure, you can store your token as a Codespaces secret
> and use the command line through Codespaces. For more information, see
> "Managing encrypted secrets for your codespaces."

```shell
curl --request GET \
--url "https://api.github.com/repos/octocat/Spoon-Knife/issues" \
--header "Accept: application/vnd.github+json" \
--header "Authorization: Bearer YOUR-TOKEN"
```

## Authenticating to the REST API

Mais comment créer ce token.

[Authenticating to the REST API](https://docs.github.com/en/rest/authentication/authenticating-to-the-rest-api?apiVersion=2022-11-28)

> Additionally, you can make more requests per hour when you are authenticated.

> To authenticate your request, you will need to provide an authentication
> token with the required scopes or permissions. 
>
> There a few different ways to get a token:
> - You can create a personal access token, 
> - generate a token with a GitHub App,
> - or use the built-in GITHUB_TOKEN in a GitHub Actions workflow.

[https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28](https://docs.github.com/en/rest/using-the-rest-api/rate-limits-for-the-rest-api?apiVersion=2022-11-28)

> The primary rate limit for unauthenticated requests is 60 requests per hour.

> Additionally, you can authorize a GitHub App or OAuth app, which can then
> make API requests on your behalf.
> All of these requests count towards your personal rate limit of 5,000
> requests per hour. 

## Managing your personal access tokens

C'est un labyrinthe :/

[Managing your personal access tokens](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens)

> GitHub currently supports two types of personal access tokens: fine-grained
> personal access tokens and personal access tokens (classic). GitHub
> recommends that you use fine-grained personal access tokens instead of
> personal access tokens (classic) whenever possible. 

<https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#creating-a-fine-grained-personal-access-token>

## GitHub REST API

<https://docs.github.com/en/rest/authentication/endpoints-available-for-fine-grained-personal-access-tokens?apiVersion=2022-11-28#issues>

```
GET /issues
GET /orgs/{org}/issues
GET /repos/{owner}/{repo}/assignees
GET /repos/{owner}/{repo}/assignees/{assignee}
GET /repos/{owner}/{repo}/issues
POST /repos/{owner}/{repo}/issues
GET /repos/{owner}/{repo}/issues/comments
GET /repos/{owner}/{repo}/issues/comments/{comment_id}
PATCH /repos/{owner}/{repo}/issues/comments/{comment_id}
DELETE /repos/{owner}/{repo}/issues/comments/{comment_id}
GET /repos/{owner}/{repo}/issues/events
GET /repos/{owner}/{repo}/issues/events/{event_id}
GET /repos/{owner}/{repo}/issues/{issue_number}
PATCH /repos/{owner}/{repo}/issues/{issue_number}
POST /repos/{owner}/{repo}/issues/{issue_number}/assignees
DELETE /repos/{owner}/{repo}/issues/{issue_number}/assignees
GET /repos/{owner}/{repo}/issues/{issue_number}/assignees/{assignee}
GET /repos/{owner}/{repo}/issues/{issue_number}/comments
POST /repos/{owner}/{repo}/issues/{issue_number}/comments
GET /repos/{owner}/{repo}/issues/{issue_number}/events
GET /repos/{owner}/{repo}/issues/{issue_number}/labels
POST /repos/{owner}/{repo}/issues/{issue_number}/labels
PUT /repos/{owner}/{repo}/issues/{issue_number}/labels
DELETE /repos/{owner}/{repo}/issues/{issue_number}/labels
DELETE /repos/{owner}/{repo}/issues/{issue_number}/labels/{name}
PUT /repos/{owner}/{repo}/issues/{issue_number}/lock
DELETE /repos/{owner}/{repo}/issues/{issue_number}/lock
GET /repos/{owner}/{repo}/issues/{issue_number}/timeline
```

## Utilisation

<https://github.com/pypi/support/issues>

```shell
$  export MY_PYPI_ISSUES_TOKEN='github_pat_xxxxxxxxxxxxxxx'

$ curl -L \
  -H "Accept: application/vnd.github+json" \
  -H "Authorization: Bearer ${MY_PYPI_ISSUES_TOKEN}" \
  -H "X-GitHub-Api-Version: 2022-11-28" \
  https://api.github.com/repos/pypi/support/issues
```

<https://docs.github.com/en/rest/issues/issues?apiVersion=2022-11-28#list-repository-issues>

## Pagination

<https://docs.github.com/en/rest/using-the-rest-api/using-pagination-in-the-rest-api?apiVersion=2022-11-28>

> When a response from the REST API would include many results, GitHub will
> paginate the results and return a subset of the results. For example, GET
> /repos/octocat/Spoon-Knife/issues will only return 30 issues from the
> octocat/Spoon-Knife repository even though the repository includes over 1600
> open issues. 

## Medium : Analyzing issue data with Github REST API

<https://blog.exploratory.io/analyzing-issue-data-with-github-rest-api-63945017dedc>

```r
pages <- list()
for(i in 1:18){
  res <- GET("https://api.github.com/repos/hadley/dplyr/issues",
             query = list(state = "all", per_page = 100, page = i))
  jsondata <- content(res, type = "text")
  github_df <- fromJSON(jsondata, flatten = TRUE)
  pages[[i]] <- github_df
}
issues <- bind_rows(pages)
issues <- as_data_frame(issues)
```

L'issue https://github.com/pypi/support/issues/2726 a été fermée par cmaureir,
mais on n'a pas le nom de la personne qui a fermé l'issue dans les données

```
grep cmaureir pypi-issues_31-05-2024.15.json
```

Il faut récupérer les "events" de l'issue : pypi-issue-events.2726.json

`GET /repos/{owner}/{repo}/issues/{issue_number}/events`

<https://docs.github.com/en/rest/issues/events?apiVersion=2022-11-28#list-issue-events>

**Ou plus simplement dans la doc de l'API GitHub**

<https://docs.github.com/en/rest/issues/issues?apiVersion=2022-11-28#list-repository-issues>

### Duplicates

J'ai des issues en double dans les différentes "pages" récupérées :
<https://github.com/orgs/community/discussions/24361>

La discussion évoque une librairie Python [PyGithub](https://github.com/pygithub/pygithub)

Autant utiliser **GitHub CLI** ?

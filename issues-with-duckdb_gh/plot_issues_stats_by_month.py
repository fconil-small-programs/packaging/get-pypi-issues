"""
https://matplotlib.org/2.0.2/users/pyplot_tutorial.html
https://matplotlib.org/2.0.2/examples/api/barchart_demo.html
"""

from pathlib import Path

import duckdb
import dotenv

# import pandas as pd
import matplotlib.pyplot as plt


def generate_date_tick(row):
    """Generate a month-date label for horizontal axis.

    ATTENTION : row will be a Series with uniform data types
    - if a column contains float64 and other columns are int64,
      all row elements will be float64
    - if a column contains strings (object), all row elements
      will be object
    See "Why data types was changed while calling the
    apply function in Pandas?" : https://stackoverflow.com/a/40315866

    :param _type_ row: A Pandas row with all columns
    :return _type_: string
    """
    return f"{row['cl_year']}-{row['cl_month']:02d}"


if __name__ == "__main__":
    # Get environment variables in an OrderedDict
    # https://github.com/theskumar/python-dotenv
    env_vars = dotenv.dotenv_values()

    # Build the path to the issues DuckDB database file
    issues_db = Path(env_vars.get("PYPI_DB_PATH")) / env_vars.get("PYPI_DB_NAME")

    # Connect to the file (no failure if nothing to expand)
    cnx = duckdb.connect(str(issues_db.expanduser()), read_only=True)

    # Load query from file
    with open(env_vars.get("QUERY_ISSUES_STATS_BY_MONTH"), "r", encoding="utf-8") as f:
        QUERY = f.read()

    # Execute query and get a Pandas DataFrame back
    # https://duckdb.org/docs/api/python/overview#result-conversion
    df = cnx.sql(QUERY).df()

    # Create a value for x tick labelling
    df["x_tick"] = df.apply(generate_date_tick, axis=1)  # , result_type="reduce")

    # fig = plt.figure(num="PyPI Issues", figsize=(12, 8), dpi=600)
    fig = plt.figure(num="PyPI Issues", figsize=(12, 8), layout="constrained")
    ax = fig.add_subplot(1, 1, 1)

    # https://matplotlib.org/stable/users/explain/colors/colors.html#colors-def
    # https://matplotlib.org/stable/users/explain/colors/colormaps.html
    ax.plot(
        df["x_tick"],
        df["nb_days_avg"],
        color="b",
        marker="o",
        label="Number of days between opening and close",
    )

    ax.plot(
        df["x_tick"],
        df["closed_nb"],
        color="g",
        marker="o",
        label="Issues closed by someone else than the opener",
    )

    ax.set_title(
        "Statistics on PyPI issues, by month",
        pad=10,
    )

    ax.legend()

    # ~/Documents/DEVLOG/Evaluations/2024/Bilan-Formations/bilan-8-global-reseau.matplotlib.py
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    plt.show()

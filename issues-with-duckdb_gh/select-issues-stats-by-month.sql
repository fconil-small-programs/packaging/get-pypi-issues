-- Compter les issues "PEP 541" fermées par d'autres personnes que celles qui
-- les ont ouvertes. Par mois

SELECT
    year(i.closed_at) AS cl_year,
    month(i.closed_at) AS cl_month,
    cast(round(avg(datediff('day', i.created_at, i.closed_at)), 0) AS BIGINT) AS nb_days_avg,
    count(*) AS closed_nb
FROM
    pypi_issues AS i
INNER JOIN pypi_issues_events AS e
    ON i.number = e.issue_number
INNER JOIN pypi_issues_labels AS l
    ON l.issue_id = i.id
WHERE
    i.state = 'closed'
    AND i.user_login != e.actor_login
GROUP BY
    cl_year, cl_month
ORDER BY
    cl_year, cl_month;

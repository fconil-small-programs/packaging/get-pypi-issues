#!/bin/bash

# https://docs.github.com/en/rest/quickstart?apiVersion=2022-11-28
# https://docs.github.com/en/rest/issues/events?apiVersion=2022-11-28#list-issue-events
#
# https://cli.github.com/manual/gh_api
# --paginate 
#   Make additional HTTP requests to fetch all pages of results

$ gh api -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" "/repos/pypi/support/issues?state=all" --paginate >pypi_issues_gh.json


#!/bin/bash

# ls -a events | while read A
# do
#     echo "Filtering closed event in events/$A"
#     jq '.[] | select(.event == "closed")' "events/$A" > "events_closed/$A"
# done

# query="INSERT INTO pypi_issues_events (
# > SELECT ${inb}, id, node_id, state, user.id,
# >        user.login, user.node_id, user.type, 
# >        user.site_admin, event, created_at, state_reason 
# > FROM read_json('events_closed/pypi_issue_${inb}_closed.json',
# >                format = 'array',
# >                columns = {'id': 'BIGINT',
# >                           'number': 'BIGINT',
# >                           'node_id': 'VARCHAR',
# >                           'state': 'VARCHAR',
# >                           'user': 'STRUCT(login VARCHAR, id BIGINT, node_id VARCHAR)',
# >                           'labels': 'STRUCT(
# >                                        id BIGINT,
# >                                        node_id VARCHAR,
# >                                        url VARCHAR,
# >                                        'name' VARCHAR,
# >                                        color VARCHAR,
# >                                        'default' BOOLEAN,
# >                                        description VARCHAR
# >                                        )[]',
# >                           'created_at': 'TIMESTAMP',
# >                           'closed_at': 'TIMESTAMP'
# >                         })
# > );"

duckdb pypi_issues.db -noheader -csv -c "SELECT number FROM pypi_issues WHERE state='closed'" | while read inb
do
    echo "Filtering closed event in events/pypi_issue_${inb}.json"
    #         format = 'array', \
    query="INSERT INTO pypi_issues_events ( \
            SELECT \
              ${inb}, \
              id, \
              node_id, \
              actor.id, \
              actor.login, \
              actor.node_id, \
              actor.type, \
              actor.site_admin, \
              event, \
              created_at, \
              state_reason \
            FROM read_json( \
              'events_closed/pypi_issue_${inb}_closed.json', \
              columns = { \
                'id': 'BIGINT', \
                'node_id': 'VARCHAR', \
                'actor': 'STRUCT(login VARCHAR, id BIGINT, node_id VARCHAR, type VARCHAR, site_admin VARCHAR)', \
                'state': 'VARCHAR', \
                'event': 'VARCHAR', \
                'created_at': 'TIMESTAMP', \
                'state_reason': 'VARCHAR' \
              } \
            ) \
          );"

    # echo $query
    duckdb pypi_issues.db -noheader -csv -c "${query}"
done

#!/bin/bash

# ls -a events | while read A
# do
#     echo "Filtering closed event in events/$A"
#     jq '.[] | select(.event == "closed")' "events/$A" > "events_closed/$A"
# done

duckdb pypi_issues.db -noheader -csv -c "SELECT number FROM pypi_issues WHERE state='closed'" | while read inb
do
    echo "Filtering closed event in events/pypi_issue_${inb}.json"
    jq '.[] | select(.event == "closed")' "events/pypi_issue_${inb}.json" > "events_closed/pypi_issue_${inb}_closed.json"
done

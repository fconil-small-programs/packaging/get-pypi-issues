#!/bin/bash

# select number from pypi_issues where state='closed' limit 5;
# gh api -H "Accept: application/vnd.github+json" -H "X-GitHub-Api-Version: 2022-11-28" "/repos/pypi/support/issues/4012/events" --paginate >pypi_issue_4012_events_gh.json

duckdb pypi_issues.db -noheader -csv -c "SELECT number FROM pypi_issues WHERE state='closed'" | while read inb
do
    echo "Get $inb issues events";
    gh api \
        -H "Accept: application/vnd.github+json" \
        -H "X-GitHub-Api-Version: 2022-11-28" \
        "/repos/pypi/support/issues/${inb}/events" \
        --paginate >"events/pypi_issue_${inb}.json"
    sleep 0.2;
done

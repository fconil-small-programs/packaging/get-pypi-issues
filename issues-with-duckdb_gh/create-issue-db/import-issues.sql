-- Define the dialect
-- sqlfluff:dialect:duckdb

-- Set a smaller indent for this file
-- sqlfluff:indentation:tab_space_size:2

-- Set keywords to be capitalised
-- sqlfluff:rules:capitalisation.keywords:capitalisation_policy:upper

-- https://duckdb.org/docs/data/json/overview
-- https://duckdb.org/docs/extensions/json
-- https://duckdb.org/2023/03/03/json

DROP TABLE IF EXISTS pypi_issues;

CREATE TABLE pypi_issues (
  id BIGINT PRIMARY KEY,
  number BIGINT UNIQUE,
  node_id VARCHAR,
  state VARCHAR,
  user_id BIGINT,
  user_login VARCHAR,
  user_node_id VARCHAR,
  labels JSON,
  created_at TIMESTAMP,
  closed_at TIMESTAMP
);

DROP TABLE IF EXISTS pypi_issues_labels;

CREATE TABLE pypi_issues_labels (
  issue_id BIGINT,
  label_id BIGINT,
  label_node_id VARCHAR,
  label_name VARCHAR,
  FOREIGN KEY (issue_id) REFERENCES pypi_issues (id)
);

DROP TABLE IF EXISTS pypi_issues_events;

CREATE TABLE pypi_issues_events (
  issue_number BIGINT,
  event_id BIGINT,
  node_id VARCHAR,
  actor_id BIGINT,
  actor_login VARCHAR,
  actor_node_id VARCHAR,
  actor_type VARCHAR,
  actor_site_admin VARCHAR,
  event VARCHAR,
  created_at TIMESTAMP,
  state_reason VARCHAR,
  FOREIGN KEY (issue_number) REFERENCES pypi_issues (number)
);

INSERT INTO pypi_issues
(
  SELECT
    id,
    number,
    node_id,
    state,
    user.id,
    user.login,
    user.node_id,
    labels,
    created_at,
    closed_at
  FROM read_json(
    'pypi_issues_gh.json',
    format = 'array',
    columns = {
      'id': 'BIGINT',
      'number': 'BIGINT',
      'node_id': 'VARCHAR',
      'state': 'VARCHAR',
      'user': 'STRUCT(login VARCHAR, id BIGINT, node_id VARCHAR)',
      'labels': 'STRUCT(
                   id BIGINT,
                   node_id VARCHAR,
                   url VARCHAR,
                   "name" VARCHAR,
                   color VARCHAR,
                   "default" BOOLEAN,
                   description VARCHAR
                   )[]',
      'created_at': 'TIMESTAMP',
      'closed_at': 'TIMESTAMP'
    }
  )
);

WITH q AS (
  SELECT
    id,
    unnest(labels) AS label
  FROM 'pypi_issues_gh.json'
)

INSERT INTO pypi_issues_labels (
  SELECT
    id as issue_id,
    label -> 'id' AS label_id,
    label -> 'node_id' AS label_node_id,
    label -> 'name' AS label_name
  FROM q)
;

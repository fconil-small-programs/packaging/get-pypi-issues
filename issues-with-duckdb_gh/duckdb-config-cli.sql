-- How do I make duckbox output show more than 40 rows?
-- https://github.com/duckdb/duckdb/discussions/10562

-- Je pensais que c'était un paramètre de configuration :
--     https://duckdb.org/docs/configuration/overview
-- mais non c'est une "dot" commande du CLI
--     https://duckdb.org/docs/api/cli/dot_commands.html
-- qui ne serait donc pas disponible pour Python
--     https://stackoverflow.com/a/76860941

.maxrows 80

#!/bin/bash

# https://duckdb.org/docs/guides/data_viewers/youplot
# https://github.com/red-data-tools/YouPlot
# ~/Logiciels/youplot

duckdb -csv -s "SELECT year(i.closed_at) AS cl_year, count(*) AS closed_nb FROM pypi_issues AS i INNER JOIN pypi_issues_events AS e ON i.number = e.issue_number INNER JOIN pypi_issues_labels AS l ON l.issue_id = i.id WHERE i.state = 'closed' AND instr(l.label_name, '541') AND i.user_login != e.actor_login GROUP BY cl_year ORDER BY cl_year" pypi_issues.db | youplot bar -d, -H -t "Issues PyPI PEP 541 fermées par une personne autre que celle qui l'a ouverte"

#        Issues PyPI PEP 541 fermées par une personne autre que celle qui l'a ouverte
#        ┌                                        ┐
#   2019 ┤■■■■ 33.0
#   2020 ┤■■■■■■■■■■■■■■■■■■ 136.0
#   2021 ┤■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ 250.0
#   2022 ┤■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ 249.0
#   2023 ┤■■■■■■■■■ 65.0
#   2024 ┤■■ 14.0
#        └                                        ┘

duckdb -csv -s "SELECT year(i.closed_at) AS cl_year, count(*) AS closed_nb FROM pypi_issues AS i INNER JOIN pypi_issues_events AS e ON i.number = e.issue_number INNER JOIN pypi_issues_labels AS l ON l.issue_id = i.id WHERE i.state = 'closed' AND instr(l.label_name, '541') AND i.user_login = e.actor_login GROUP BY cl_year ORDER BY cl_year" pypi_issues.db | uplot bar -d, -H -t "Issues PyPI PEP 541 fermées par la personne qui l'a ouverte"

#       Issues PyPI PEP 541 fermées par la personne qui l'a ouverte
#        ┌                                        ┐
#   2019 ┤■■ 4.0
#   2020 ┤■■■■■■■■■■■■■■■■■■■■■■■ 45.0
#   2021 ┤■■■■■■■■■■■■■■■■■■■■■■■■■■ 50.0
#   2022 ┤■■■■■■■■■■■■■■■■■■■■■■■■■■ 50.0
#   2023 ┤■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■ 66.0
#   2024 ┤■■■■■■■■■■■■■■■■■■ 34.0
#        └                                        ┘

# Get PyPI issues

Get [PyPI issues](https://api.github.com/repos/pypi/support/issues?state=all&per_page=100) to analyze PEP 541 closed issues.


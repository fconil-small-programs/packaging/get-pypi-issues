#!/bin/bash

# https://blog.exploratory.io/analyzing-issue-data-with-github-rest-api-63945017dedc
# (state = "all", per_page = 100, page = i
#    -o pypi-issues_31-05-2024.${page}.json \

# Ou plus simplement dans la doc de l'API GitHub
# https://docs.github.com/en/rest/issues/issues?apiVersion=2022-11-28#list-repository-issues

seq -w 1 42 | while read page
do
    curl -s -L -H "Accept: application/vnd.github+json" \
        -H "Authorization: Bearer ${MY_PYPI_ISSUES_TOKEN}" \
        -H "X-GitHub-Api-Version: 2022-11-28" \
        -o pypi_issues.${page}.json \
        "https://api.github.com/repos/pypi/support/issues?state=all&per_page=100&page=${page}"
done
